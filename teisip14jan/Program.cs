﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace teisip14jan
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene i;
            i = new Inimene {Nimi = "Henn", Kaasa = new Inimene {Nimi = "Maris" }}; //tekivad string nimi ja int vanus klassis Inimene. Kui täidetakse new operatsioon siis 

            Console.WriteLine(i);
            Console.WriteLine(i.Kaasa.Kaasa.Nimi?? "Kaasat veel ei ole");//? tähendab et pöördu olematu asja nime poole. . on member call tehe
            
            Inimene i2;
            i2 = new Inimene(); // tekib teine komplekt

         

            //i?.Kaasa on sama mis i== null ? null : i.Kaasa (Kui i väärtuseks on 0, siis avalduse väärtuseks on 0 ja kui on avaldise väärtus siis anna see.

            //Console.Writeline (i.Kaasa?.Nimi??"Kaasat veel ei ole"); ?? n coalesce operatsioon, kui avaldise väärtus on 0, siis asenda see väärtusega. Sama mis av == null ? "väärtus" : av

            var x = i?.Vanus;
            int? arv1 = 0; //-2miljardit kuni +2miljardit, ?-ga lisandub neile null väärtus (e. tühi väärtus)
            int? arv2 = 0;
            var tulemus = arv1 + arv2; //int? tüüpi, aga var tulemus = arv1??0 + arv2??0 väärtus on int tüüpi.cbxd

        }
    }

    class Inimene
    {
        public static int Inimestearv = 0; //

        public string Nimi; // Vaikimisi ehk default väärtused stringidel on "" ehk tühi string (olemuselt sama mis null e. tühimik)
        public int Vanus; //Vaikimisi ehk default väärtused arvudel on 0.
        public Inimene Kaasa;// vaikimisi väärtus on null e. tühimik.

        public override string ToString()=> $"{Nimi} vanusega {Vanus} aastat.";
        
    }
}
